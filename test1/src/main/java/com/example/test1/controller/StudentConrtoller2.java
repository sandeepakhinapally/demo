package com.example.test1.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.test1.model.Student;
import com.example.test1.service.impl.StudentServiceImpl;

@RestController
@RequestMapping("/student2")
public class StudentConrtoller2 {

//	@Autowired // beans-> which are annotated with service, controller or repository or
//				// component
//	StudentService studentService;

	StudentServiceImpl studentServiceImpl = new StudentServiceImpl();// instance

	@GetMapping("/getStudentDetails/{studentId}")
	public Student getStudentDetails(@PathVariable(name = "studentId") String studentId) {
		System.out.println(studentServiceImpl);
		return studentServiceImpl.getStudentDetails(studentId);
	}
}
