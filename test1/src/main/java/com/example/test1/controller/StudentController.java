package com.example.test1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.test1.domain.StudentJson;
import com.example.test1.model.Student;
import com.example.test1.service.StudentService;

@RestController
@RequestMapping("/students")
public class StudentController {

	@Autowired
	StudentService studentService;

	@GetMapping("/getStudentDetails/{studentId}")
	public ResponseEntity<Object> getStudentDetails(@PathVariable(name = "studentId") String studentId) {
		Student s = studentService.getStudentDetails(studentId);
		if (null != s) {
			return new ResponseEntity<>(s, HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Details not found for given id", HttpStatus.OK);
		}
	}

	@PostMapping("/addStudent")
	public ResponseEntity<String> addStudentDetails(@RequestBody StudentJson student) {
		boolean isStudentAdded = studentService.addStudent(student);
		if (isStudentAdded) {
			return new ResponseEntity<>("Student added to table", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Student already present or not added due to some technical glitch",
					HttpStatus.OK);
		}
	}

	@DeleteMapping("/deleteStudentDetails/{studentId}")
	public ResponseEntity<Object> deleteStudentDetails(@PathVariable(name = "studentId") String studentId) {
		String s = studentService.deleteStudentDetails(studentId);
		return new ResponseEntity<>(s, HttpStatus.OK);

	}

}
