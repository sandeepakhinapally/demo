package com.example.test1.service;

public interface EnrollmentDashboardService {

	public Object getEnrolledHealthPlans(String hbxCaseId, int year);
}
