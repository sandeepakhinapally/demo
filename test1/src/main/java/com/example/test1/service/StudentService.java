package com.example.test1.service;

import com.example.test1.domain.StudentJson;
import com.example.test1.model.Student;

public interface StudentService {

	public Student getStudentDetails(String studentId);// declaration

	public boolean addStudent(StudentJson student);

	public String deleteStudentDetails(String studentId);
}
