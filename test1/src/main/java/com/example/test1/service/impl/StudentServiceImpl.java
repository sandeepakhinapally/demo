package com.example.test1.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.test1.domain.StudentJson;
import com.example.test1.model.Student;
import com.example.test1.repository.StudentRepository;
import com.example.test1.service.StudentService;

@Component
public class StudentServiceImpl implements StudentService {

	@Autowired
	StudentRepository studentRepository;

	@Override
	public Student getStudentDetails(String studentId) {

		Student s = studentRepository.findById(studentId).orElse(null);

		if (null != s && null != s.getStudentId() && s.getStudentId().equals(studentId)) {
			return s;
		} else {
			return null;
		}

	}

	@Override
	public boolean addStudent(StudentJson studentJson) {
		Student alreadyExistingStudent = studentRepository.findById(studentJson.getStudentId()).orElse(null);
		if (null != alreadyExistingStudent) {
			return false;
		} else {
			Student student = new Student(studentJson.getStudentId(), studentJson.getStudentName(),
					studentJson.getStudentEmail());
			Student studentAdded = studentRepository.save(student);
			if (null != studentAdded) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String deleteStudentDetails(String studentId) {
		boolean isStudentExisting = studentRepository.existsById(studentId);
		if (isStudentExisting) {
			studentRepository.deleteById(studentId);
			return "Student details deleted";
		} else {
			return "Student details not found";
		}

	}

}
