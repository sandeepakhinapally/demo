package com.example.test1.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StudentJson {

	@JsonProperty("student_id")
	private String studentId;

	@JsonProperty("student_name")
	private String studentName;

	@JsonProperty("student_email")
	private String studentEmail;

	public StudentJson() {
		super();
	}

	public StudentJson(String studentId, String studentName, String studentEmail) {
		super();
		this.studentId = studentId;
		this.studentName = studentName;
		this.studentEmail = studentEmail;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getStudentEmail() {
		return studentEmail;
	}

	public void setStudentEmail(String studentEmail) {
		this.studentEmail = studentEmail;
	}

}
